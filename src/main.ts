import { Core } from "@intutable/core"
import { join as joinPath } from "path"

async function main() {
    await Core.create([
        joinPath(__dirname, "../node_modules/@intutable/*"),
        joinPath(__dirname, ".."),
    ])
}
main()
